---
title: "Events"
date: 2020-06-14T19:32:20+02:00
draft: true
---
## Server Events

### Retrieve Roleplayer

Returns the roleplay player (yeah a mouthful, we know haha). Can be used to get a job, or someone's money. Click here for details on the "Roleplayer" class

```lua
TriggerEvent('redemrp:getPlayerFromId', source, function(user)
    -- Code
end)
```

### Retrieve Player

Returns the base user object. Can be used to get permission level, or someone's group. Click here for details on the "Player" class

```lua
TriggerEvent('redem:getPlayerFromId', source, function(user)
    -- Code
end)
```