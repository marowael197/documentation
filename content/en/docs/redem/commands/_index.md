---
title: "Commands"
date: 2020-06-14T19:32:20+02:00
draft: true
---

{{% alert title="Notice" color="notice" %}}
You need to have admin commands enabled for this to work.
{{% /alert %}}

### Available commands
```lua
-- Parameters: id count
/setgold

-- Parameters: id count
/setmoney

-- Parameters: id count
/setlevel

-- Parameters: id count
/setxp

-- Parameters: id name grade
/setjob

-- Parameters: id count
/addmoney

-- Parameters: id count
/addgold

-- Parameters: id count
/addxp

-- Parameters: id count
/removemoney

-- Parameters: id count
/removegold

-- Parameters: id groupname
/setgroup
```