---
title: "Inventory"
date: 2020-06-14T19:32:20+02:00
draft: true
---

### Introduction
Created by Ktos & Amakuu to provide an inventory system to RedEM:RP. Can be found at [https://github.com/RedEM-RP/redemrp_inventory](https://github.com/RedEM-RP/redemrp_inventory)

{{% alert title="Notice" color="primary" %}}
All code in this documentation was written on serverside scripts
{{% /alert %}}

### Events
To get inventory data there is an event you can call.

```lua
-- Serverside
-- Data will contain the functions needed to obtain inventory information and write inventory information
data = {}
TriggerEvent("redemrp_inventory:getData",function(call)
    data = call
end)
```

### Functions

Then to utilize the data object you can do so like this
```lua
-- Deletes an item from someones inventory
data.delItem(source, "itemname", count)

-- Adds an item to someone's inventory
data.addItem(source, "itemname", count)

-- Returns the count of "itemname" if it exists
data.checkItem(source, "itemname")
```

It's also possible to add and remove weaponry from someone's inventory using the following functions
```lua
-- If you'd like to add a weapon, use this function
data.addItem(source, "itemname", ammo, WeaponHash)

-- If you'd like to delete a weapon, use this function
data.delItem(source, "itemname", ammo, WeaponHash)
```