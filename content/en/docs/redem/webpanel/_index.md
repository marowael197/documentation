---
title: "RedEM: Webpanel"
linkTitle: "Webpanel"
---

This panel was created to suit a certain need within the community, this web panel allows you to manage multiple RedM servers from the comfort of anywhere in the world via a simple web browser.

## Features
Very unique features are marked in **bold**

* **Multi server management**
* Kick people
* Set their job
* Set their jobgrade
* Set their money
* Set their gold
* **Edit the database directly**
* Stop the server
* \+ more to come!

## Access
Currently to gain access you have to be a patron, our Patreon can be found [here](https://patreon.com/gdevelopment). This will probably be lifted in the future once we have enough Patrons to properly support our infrastructure. Once you became a Patron you can find the panel by clicking [here](https://api.kanersps.pw/redem/premium)

## Screenshots

| Multi server management view      |
| ------------- |
| ![](https://i.kanersps.pw/pNJRjiy1kesFiWD) |

| Dashboard      |
| ------------- |
| ![](https://i.kanersps.pw/o4fXiiTy5YDF7Xe) |

| User database editor        |
| ------------- |
| ![](https://i.kanersps.pw/-v71nrOlO_ThUZp) |

| Player Cards        |
| ------------- |
| ![](https://i.kanersps.pw/uhPPm2hYY15MKTN) |
