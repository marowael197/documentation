---
title: "Roleplayer"
date: 2020-06-14T19:32:20+02:00
draft: true
---

### Structure
```lua
{
	self.source = source
	self.gold = gold
	self.group = group
	self.firstname = firstname
	self.lastname = lastname
	self.xp = xp
	self.level = level
	self.job = job
	self.jobgrade = jobgrade
}
```

### Functions
Here we are assuming that the "Player" class is retrieved with [redemrp:getPlayerFromId](/redem/events/#retrieve-roleplayer) so that we have access to "user"
```lua
TriggerEvent("redemrp:getPlayerFromId", playerId, function(user)
    user.setLevelwXP(value)
    user.setLevel(value)
    user.setXP(value)
    user.setFirstname(value)
    user.setLastname(value)
    user.setJob(value)
    user.setJobgrade(value)
    user.addXP(value)
    user.setGold(value)
    user.getGold()
    user.kick(reason)
    user.addMoney(reason)
    user.removeMoney(reason)
    user.addGold(reason)
    user.removeGold(reason)
    user.getXP()
    user.getName()
    user.getLevel()
    user.getFirstname()
    user.getLastname()
    user.getJob()
    user.getJobgrade()
end)
```

### Example
```lua
TriggerEvent('redemrp:getPlayerFromId', source, function(user)
    -- Get the users identifier and stores it as a local variable named "identifier"
    local identifier = user.getIdentifier()

    -- Gets the character id and stores it as a local variable named "charid"
    local charid = user.getSessionVar("charid")

    -- Set money to 500
    user.setMoney(500)
end)
```