---
title: "User Class"
---

### Structure

```lua
-- Getters
user.getMoney() -- Returns number
user.getBank() -- Returns number
user.getCoords() -- Returns table(coords.x, coords.y, coords.z)
user.getSessionVar(key) -- Returns stored session variable
user.getPermissions() -- Returns int
user.getIdentifier() -- Returns user identifier, most likely hex steamid64
user.getGroup() -- Returns string of a users group
user.get('item') -- Can be used for all of the above
 
-- Setters
user.setMoney(amount) -- Sets the user money
user.setBankBalance(amount) -- Sets the user bank balance
user.removeMoney(amount) -- Removes from the current amount of user money
user.addMoney(amount) -- Adds to the current amount of user money
user.removeBank(amount) -- Removes from the current amount of user bank balance
user.addBank(amount) -- Adds to the current amount of user bank balance
 
-- Functions
user.displayMoney(amount) -- Used to display amount of money in UI
user.displayBank(amount) -- Same as above for bank, only use if using NativeUI option
user.kick(reason) -- Kicks user with specified reason
 
-- Roles
user.addRole(role) -- Adds a role to a user
user.removeRole(role) -- Removes a role from a user
user.hasRole(role) -- Returns true/false depending on if the user has the specified role
```

### Globals

Allows you to dynamically make your own wrappers

```lua
-- Sets a global function on a user, call only once like on es:playerLoaded
user.setGlobal('global', 'default') -- See example below
 
user.setGlobal('job', 'citizen')
-- Will create 2 new functions on the user
user.setJob(value) -- Sets the global 'job' to value

user.getJob() -- Gets the global 'job'
```