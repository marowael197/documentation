---
title: "4.6.3"
weight: 930
---


25 Aug 2017 
### Changelog

    Added convar: es_enableDevTools enabled by default. Set to 0 if you want them disabled
    Async write fixes while logging
    Added license, add this to your mysql database if you don’t have it yet or use the new sql file that comes with the plugin
    SteamID check when logging a user in (kicks if it doesn’t exist)
    Load user with license instead of steamid if the license is found but the steamid isn’t
    Code cleanup

### EssentialMode admin update(s)

    When banning it will ban their: steamid, license and ip

### Events

No changes

**Compatibility issues**

No changes