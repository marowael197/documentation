---
title: "4.0.0"
weight: 960
---


21 Jun 2017 

### Changelog

    FXServer compatibility
    Native money UI option
    Automated update checks
    Code fixes
    Server exports
    Convars (no edits needed to any of the core files)
    DB code updates

### User class update

Player ‘class’ has been edited to fix an issue with metatables, as I abused them in NeoLua. So the ‘:’ has been replaced with ‘.’ example:
```
user:setMoney(500)
-- needs to be
user.setMoney(500)
```
### Events

No changes

**Compatibility issues**

Not compatible with CitizenMP.Server anymore


