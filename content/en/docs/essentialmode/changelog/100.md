---
title: "1.0.0"
weight: 990
---


08 Feb 2017

From now on EssentialMode is no longer a gamemode, this means that the player won’t spawn if you don’t have another one loaded. There is an example gamemode that comes with an example of how to properely spawn a player wherever you want. This means no more teleporting wherever you want the player to actually spawn.
### Changelog

    Ban length & Proper ban table.
    Cleaner code

Included

    EssentialMode
    Admin plugin
    Example gamemode

### Events
**New**
```lua
-- Gets called when a user command is successfully ran.
AddEventHandler('es:userCommandRan', function(source, command_args, user) end)

-- Gets called when any command is ran
AddEventHandler('es:commandRan', function(source, command_args, user) end)

-- Gets called when an admin command is ran
AddEventHandler('es:adminCommandRan', function(source, command_args, user) end)

-- Gets called when a non existant command is ran. (This can be cancelled to not display the message)
AddEventHandler('es:invalidCommandHandler', function(source, command_args, user) end)

-- This gets called when an admin command is ran, but the user does not have permission to run it.
AddEventHandler('es:adminCommandFailed', function(source, command_args, user) end)

-- This gets called when a chat message is send.
AddEventHandler('es:chatMessage', function(source, command_args, user) end)
```

**Compatibility issues**

This update will not be plug & play compatible with older versions, please use the new SQL.


