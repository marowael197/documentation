---
title: "1.1.0"
weight: 980
---


22 Feb 2017

This update has one main thing which I thought was quite important, essentialmode has default settings now. These settings can be changed by any plugin but they are actually meant to be changed by a gamemode and not by a plugin. Plugins can utilize the session settings.
### Changelog

    Default settings
    Session settings

**Default settings**
```lua
settings.defaultSettings = {
	['banReason'] = "You are currently banned. Please go to: insertsite.com/bans", -- The default ban reason, can be a function aswell. The function that gets called has two parameters: function(identifier, username)
	['pvpEnabled'] = false, -- Do you want server-wide pvp
	['permissionDenied'] = false, -- Can be set to a string, this will be displayed if a user tries to run a command it does not have permission to.
	['debugInformation'] = false -- Do you want to log debug information
}
```
### Events
**Changed**

-- When this event gets cancelled now it won't display the default permission denied message.
AddEventHandler('es:adminCommandFailed', function(source, command_args, user) end)

**New**

-- You can set the default settings with this. The parameter is a table, the settings you want to change can be put in here.
TriggerEvent("es:setDefaultSettings", {})
-- Here is an example usage
TriggerEvent("es:setDefaultSettings", {
    pvpEnabled = true -- Default false
})

-- This is able to set a session setting, this setting is saved until server restart.
TriggerEvent("es:setSessionSetting", key, value)

-- With this you can get the variable that was saved using setSessionSetting, the callback has one parameter which contains the stored value (or nil).
TriggerEvent("es:getSessionSetting", key, callback)

**Compatibility issues**

No changes