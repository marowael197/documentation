---
title: "Documentation"
---

Here you can find all the documentation I wrote for public projects of mine, please select a page on the left to start. Or explore the most popular pages listed here on the bottom!

### Popular Pages

<div style="display: flex;">
<div style="width: 50%; margin: 5px; padding: 10px; display: inline-block; float: left; border: 1px solid #cccccc;">
<h5>RedEM: Roleplay</h5>
<ul>
    <li><a href="redem/installation">Installation</a></li>
    <li><a href="redem/config">Configuration</a></li>
    <li><a href="redem/commands">Commands</a></li>
<ul>
</div>
<div style="width: 50%; margin: 5px; padding: 10px; display: inline-block; float: left; border: 1px solid #cccccc;">
<h5>EssentialMode</h5>
<ul>
    <li><a href="essentialmode/installation">Installation</a></li>
    <li><a href="essentialmode/es_admin">Admin System</a></li>
    <li><a href="essentialmode/reference/settings">Configuration</a></li>
<ul>
</div>
</div>

